@extends('layouts.app')

@section('content')
<div class="col-lg-6 col-md-8 mx-auto">
    <h1 class="fw-light">Album example</h1>
    <p class="lead text-muted">This is Laravel Application</p> 
    <p>
      <a href="/login" class="btn btn-primary my-2">Login</a>
      <a href="/register" class="btn btn-secondary my-2">Register</a>
    </p>
  </div>
@endsection
